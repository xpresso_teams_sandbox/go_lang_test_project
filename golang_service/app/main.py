"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import json
import logging
from json import JSONDecodeError
from flask import Flask
from flask import request

# Following import are required for Xpresso. Do not remove this.
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="golang_service",
                   level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/')
def hello_world():
    """
    Send response to Hello World
    """

    logger.info("Received request from {}".format(request.remote_addr))
    try:
        logger.info("Processing the request")
        cfg_fs = open(config_file, 'r')
        config = json.load(cfg_fs)
        project_name = config["project_name"]
        logger.info("Request Processing Done")
        logger.info("Sending Response to {}".format(request.remote_addr))
        return '<html><body><b>Hello World from {}!</b></body></html>'.format(
            project_name
        )
    except (FileNotFoundError, JSONDecodeError):
        logger.error("Request Processing Failed")
        logger.info("Sending Default Response")
        return '<html><body><b>Hello World!</b></body></html>'


if __name__ == '__main__':
    app.run(host="0.0.0.0")
